// paperscript
'use strict';
function onFrame(ev) { }

document.getElementById('controls').innerHTML = ' <label><input type="checkbox" checked id="red">Red</input></label>\
<label><input type="checkbox" checked id="green">Green</input></label>\
<label><input type="checkbox" checked id="blue">Blue</input></label>'

var backg = new Path.Rectangle({
    point: [0, 0],
    size: [805, 605],
    fillColor: { red: 1, green: 1, blue: 1 }
});

var block = new Path({
    segments: [[200, 225], [200, 375]],
    strokeColor: 'grey',
    strokeWidth: 2
});

function Lamp(pos, color) {
    pos = new Point(pos);
    var slope1 = (pos.y - 225)/(pos.x - 200);
    var slope2 = (pos.y - 375)/(pos.x - 200);
    this.color = color;
    this.lamp = new Path.Circle({
            center: pos,
            radius: 20,
            strokeColor: 'black',
            fillColor: color
    });
    this.shadow = new Path({
        segments: [[200, 225], [0, 225-200*slope1], [0, 375-200*slope2], [200, 375]],
        fillColor: color,
        blendMode: 'subtract'
    });
    this.guide = new Path({
        segments: [[200, 225], pos, [200, 375]],
         strokeColor: 'grey',
        dashArray: [2, 5]
    });
    var a = this;
    this.lamp.onMouseDrag = function(ev) {
        if(ev.point.x < 315) {
            ev.point.x = 315;
        } else if(ev.point.x > 800) {
            ev.point.x = 800;
        }
        if(ev.point.y < 0) {
            ev.point.y = 0;
        } else if(ev.point.y > 600) {
            ev.point.y = 600;
        }
        this.position = ev.point;
        var slope1 = (this.position.y - 225)/(this.position.x - 200);
        var slope2 = (this.position.y - 375)/(this.position.x - 200);
        a.guide.segments = [[200, 225], this.position, [200, 375]];
        a.shadow.segments = [
            [200, 225], [0, 225-200*slope1],
            [0, 375-200*slope2], [200, 375]
        ];
    }
}

Lamp.prototype.turnOff = function() {
    this.lamp.fillColor = 'black';
    this.shadow.fillColor = null;
    this.shadow.blendMode = null;
    this.guide.strokeColor = null;
}

Lamp.prototype.turnOn = function() {
    this.lamp.fillColor = this.color;
    this.shadow.fillColor = this.color;
    this.shadow.blendMode = 'subtract';
    this.guide.strokeColor = 'grey';
}

var redlamp = new Lamp([700, 150], { red:1 });
var greenlamp = new Lamp([700, 300], { green: 1 });
var bluelamp = new Lamp([700, 450], { blue: 1 });

document.getElementById('red').onclick = function() {
    if(this.checked) {
        backg.fillColor.red = 1;
        redlamp.turnOn();
    } else {
        backg.fillColor.red = 0;
        redlamp.turnOff();
    }
}

document.getElementById('green').onclick = function() {
    if(this.checked) {
        backg.fillColor.green = 1;
        greenlamp.turnOn();
    } else {
        backg.fillColor.green = 0;
        greenlamp.turnOff();
    }
}

document.getElementById('blue').onclick = function() {
    if(this.checked) {
        backg.fillColor.blue = 1;
        bluelamp.turnOn();
    } else {
        backg.fillColor.blue = 0;
        bluelamp.turnOff();
    }
}
