# Physical Science Models

This repository contains applications designed for use in science classrooms.

The models are
 * Thin Lens Simulation
 * Thin Mirror Simulation
 * Color Mixing
 * Colored Shadows

The simulations will be written in javascript using the
[Paper.js](https://paperjs.org) library to run in an html5 capable web browser
on a desktop or on mobile devices such as tablets. They are free to use for
anyone.

These applications are coursework for MATH497 independent study at Minnesota 
State University Moorhead

## Installation

To install the simulations to your personal or faculty site, just copy the html
and js files, the style.css file, and the paper directory along with its
contents to a location accessible by your web server.

If you wish to include the simulations in lecture notes or a custom page, add
the following to the html page you want to embed them in:
```html
<canvas id="sim" width=800 height=600 style="background-color:white;"></canvas>
<div id="controls"></div>
```
Then include the appropriate scripts for the simulation in either the `<head>`
or at the end of the `<body>`:
```html
<script type="text/javascript" src="paper/paper-full.js"></script>
<script type="text/paperscript" canvas="sim" src="thin-lens.js"></script>
```
The script for each simulation will automatically add the necessary controls to
the `<div>` when the page loads. Further customisation can be accomplished by
for example using absolute positioning on the elements.

### Updating

Simply copy the newer versions of the files over the old versions

### Demonstration

The simulations can be viewed at https://boltsj.gitlab.io/psci-models/. Please
note that GitLab pages can have long load times if their servers are under heavy
load and the preferred method to deliver the simulations to your students is to
add them to your faculty web page.

## Copyright Information

Copyright (C) 2016  Joseph R. Nosie

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Paper.js

Included Paper.js distribution files are copyright their respective creators
and licensed separately. Paper.js license information can be found in the paper
subdirectory of this project.
