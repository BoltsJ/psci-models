// paperscript
'use strict';

document.getElementById('controls').innerHTML = '<span id="focus"></span>,<br />\
focal length = <input type="number" min="50" max="400" pattern"\d+" id="flen">mm <br />\
<button id="concave">Concave</button><button id="convex">Convex</button><br />\
object height: <input type="range" id="height" min="20" max="150" value=100>';

function Simulation(n, fl, convex, o) {
    this._n = n;
    this._fl = fl;
    this._rp = 0;
    this._rpp = 0;
    this._convex = convex;
    this._o = o;
    this._h = 75;
    this._i = 0;
    this._m = 1;

    var sim = this;
    this._odrag = new Path.Rectangle({
        point: [380-this._o, 300],
        size: [40, -150],
        fillColor: 'white',
        onMouseDrag: function(ev) {
            this.position.x += ev.delta.x;
            if(this.position.x < 5) {
                this.position.x = 5;
            }
            if(this.position.x > 390) {
                this.position.x = 390;
            }
            sim._o = 400 - this.position.x;
            sim._draw();
        }
    });

    this._axis = new Path({
        segments: [[0, 300], [800, 300]],
        strokeColor: 'lightblue',
        dashArray: [5, 5]
    });
    this._a = new Path({ strokeColor: 'black' });
    this._b = new Path({ strokeColor: 'green' });
    this._f1 = new Path({ strokeColor: 'red' });
    this._f2 = new Path({ strokeColor: 'green' });
    this._xreal = new Path({ strokeColor: 'green' });
    this._yreal = new Path({ strokeColor: 'red' });
    this._zreal = new Path({ strokeColor: 'blue' });
    this._xvirt = new Path({
        strokeColor: 'green',
        dashArray: [1, 5]
    });
    this._yvirt = new Path({
        strokeColor: 'red',
        dashArray: [1, 5]
    });
    this._zvirt = new Path({
        strokeColor: 'blue',
        dashArray: [1, 5]
    });
    this._lens = new Path({
        strokeColor: 'grey',
        fillColor: 'grey'
    });
    this._draw();
}

Simulation.prototype._foc_points = function() {
    this._f1.removeSegments();
    this._f1.addSegments([[400-this._f, 295], [400-this._f, 305]]);
    this._f2.removeSegments();
    this._f2.addSegments([[400+this._f, 295], [400+this._f, 305]]);
}

Simulation.prototype._rays = function() {
    this._rayX();
    this._rayY();
    this._rayZ();
}

Simulation.prototype._rayX = function() {
    var slope = this._h/this._f;
    this._xreal.removeSegments();
    this._xvirt.removeSegments();
    if(slope < -1) {
        this._xreal.addSegments([
                [400-this._o,300-this._h],
                [400,300-this._h],
                [400-this._f+300/slope,0]
                ]);
    } else if(slope > 1) {
        this._xreal.addSegments([
                [400-this._o, 300-this._h],
                [400,300-this._h],
                [400-this._f-300/slope,600]
                ]);
    } else {
        this._xreal.addSegments([
                [400-this._o,300-this._h],
                [400,300-this._h],
                [0,300-this._h+400*slope]
                ]);
    }
    if(!this._convex && this._o <= this._f) {
        this._xvirt.addSegments([
                [400,300-this._h],
                [400-this._i,300-this._h*this._m],
                [400-this._i+300/slope-this._m*this._f,0]
                ]);
    } else if(this._convex) {
        this._xvirt.addSegments([
                [400-this._f, 300],
                [400,300-this._h]
                ]);
    }
}

Simulation.prototype._rayY = function() {
    this._yreal.removeSegments();
    this._yvirt.removeSegments();
    if(Math.abs(this._m*this._h) > 200) {
    } else {
        this._yreal.addSegments([
                [400-this._o,300-this._h],
                [400,300-this._h+this._h*this._o/(this._o-this._f)],
                [0,300-this._h+this._h*this._o/(this._o-this._f)]
                ]);

        if(!this._convex && this._o < this._f) {
            this._yvirt.addSegments([
                    [400-this._f,300],
                    [400,300-this._h*this._m],
                    [800,300-this._h*this._m]
                    ]);
        } else if(this._convex) {
            this._yvirt.addSegments([
                    [400-this._f,300],
                    [400,300-this._h*this._m],
                    [800,300-this._h*this._m]
                    ]);
        } else {
        }
    }
}

Simulation.prototype._rayZ = function() {
    var slope = this._h/this._o;
    this._zreal.removeSegments();
    this._zvirt.removeSegments();
    if(slope > .75) {
        this._zreal.addSegments([
                [400-this._o, 300-this._h],
                [400,300],
                [400-300/slope, 600]
                ]);
    } else {
        this._zreal.addSegments([
                [400-this._o, 300-this._h],
                [400,300],
                [0, 300-this._h+(400+this._o)*slope]
                ]);
    }
    if(!this._convex && this._o <= this._f) {
        this._zvirt.addSegments([
                [400,300],
                [400+300/slope, 0]
                ]);
    } else if(this._convex) {
        if(slope > .75) {
            this._zvirt.addSegments([
                    [400,300],
                    [400+300/slope, 0]
                    ]);
        } else {
            this._zvirt.addSegments([
                    [400,300],
                    [800, 300+this._h-(400+this._o)*slope]
                    ]);
        }
    }
}

Simulation.prototype._draw = function() {
    if(!this._convex) {
        this._f = this._fl;
        this._lens.removeSegments();
        this._lens.moveTo([395,100]);
        this._lens.arcTo([400,300],[395,500]);
        this._lens.lineTo([400,500]);
        this._lens.arcTo([410,300],[400,100]);
        this._lens.lineTo([395,100]);
    } else {
        this._f = -this._fl;
        this._lens.removeSegments();
        this._lens.moveTo([405,100]);
        this._lens.arcTo([395,300],[405,500]);
        this._lens.lineTo([410,500]);
        this._lens.arcTo([405,300],[410,100]);
        this._lens.lineTo([405,100]);
    }
    //this._f = 1/((this._n-1)*(1/this._rp-1/this._rpp));
    this._foc_points();
    this.Move(0);
}

Simulation.prototype.Move = function(dx) {
    if(dx < 15-this._o) {
        return;
    }
    if(dx > 395-this._o) {
        return;
    }
    this._o += dx;
    if(this._o === this._f) {
        this._o++;
    }
    this._i = 1/(1/this._f-1/this._o);
    this._m = -this._i/this._o;
    this._a.segments = [[200, 300], [200, 300-this._h], [207, 315-this._h],
        [200, 300-this._h], [193, 315-this._h]];
    this._a.position = [400-this._o, 300-this._h/2];
    if(Math.abs(this._i) > 450) {
        this._b.removeSegments();
    } else {
        this._b.segments = [[200, 300], [200, 300-this._h], [207, 315-this._h],
            [200, 300-this._h], [193, 315-this._h]];
        this._b.scale(this._m);
        this._b.position = [400-this._i, 300-(this._h/2)*this._m];
    }
    this._rays();
    document.getElementById('focus').innerHTML = 'f = ' +
        this._f.toFixed(2) + 'mm, o = ' +
        this._o.toFixed(0) + 'mm, i = ' + this._i.toFixed(2) +
        'mm, m = ' + this._m.toFixed(3);
}

Simulation.prototype.toggleLens = function() {
    this._convex = !this._convex;
    this._draw();
}

Simulation.prototype.setConcave = function() {
    this._convex = false;
    this._draw();
}

Simulation.prototype.setConvex = function() {
    this._convex = true;
    this._draw();
}

var o = 200;
var s = new Simulation(1.65, 75, true, o);

var flbox = document.getElementById('flen')
flbox.value = s._fl;
flbox.onchange = function() {
    if(flbox.value < 50) {
        flbox.value = 50;
    } else if(flbox.value > 400) {
        flbox.value = 400;
    }
    s._fl = Number(flbox.value);
    s._draw();
}

var hslider = document.getElementById('height');
hslider.value = s._h;
hslider.onchange = function() {
    s._h = Number(hslider.value);
    s._draw();
}

function onFrame(ev) { }

document.getElementById('concave').onclick = function() {
    s.setConcave();
}

document.getElementById('convex').onclick = function() {
    s.setConvex();
}
