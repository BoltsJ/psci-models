//paperscript
'use strict';
function onFrame(ev) { }

document.getElementById('controls').innerHTML = '\
<label><input type="radio" name="mode" id="mode" value="add" checked>Additive</input></label>\
<label><input type="radio" name="mode" id="mode" value="sub">Subtractive</input></label>'

var backg = new Path.Rectangle({
    point: [0, 0],
    size: [805, 605],
    fillColor: { red: 0, green: 0, blue: 0 }
});

var redspot = new Path.Circle({
    center: [400, 150],
    radius: 100,
    fillColor: { red: 1, green: 0, blue: 0 },
    strokeColor: 'none',
    blendMode: 'add',
    onMouseDrag: function(ev) { this.position = ev.point; }
});

var greenspot = new Path.Circle({
    center: [475, 250],
    radius: 100,
    fillColor: { red: 0, green: 1, blue: 0 },
    strokeColor: 'none',
    blendMode: 'add',
    onMouseDrag: function(ev) { this.position = ev.point; }
});

var bluespot = new Path.Circle({
    center: [325, 250],
    radius: 100,
    fillColor: { red: 0, green: 0, blue: 1 },
    strokeColor: 'none',
    blendMode: 'add',
    onMouseDrag: function(ev) { this.position = ev.point; }
});

document.getElementsByName('mode')[0].onchange = function() {
    backg.fillColor = { red: 0, green: 0, blue: 0 };
    redspot.blendMode = 'add';
    greenspot.blendMode = 'add';
    bluespot.blendMode = 'add';
};

document.getElementsByName('mode')[1].onchange = function() {
    backg.fillColor = { red: 1, green: 1, blue: 1 };
    redspot.blendMode = 'subtract';
    greenspot.blendMode = 'subtract';
    bluespot.blendMode = 'subtract';
};
